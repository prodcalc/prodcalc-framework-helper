<?php

namespace ProdCalcFrameworkHelper\Log;

class TextLogger
{
    private $logger;
    private $loggerName;
    private $loggerFilePath;

    public function setLoggerName(

        $loggerName

    ) {

        $t = $this;

        $t->loggerName = $loggerName;

        return $t;
    }

    public function getLoggerName()
    {

        $t = $this;

        return $t->loggerName;
    }

    public function setLoggerFilePath(

        $loggerFilePath

    ) {

        $t = $this;

        $t->loggerFilePath = $loggerFilePath;

        return $t;
    }

    public function getLoggerFilePath()
    {

        $t = $this;

        return $t->loggerFilePath;
    }

    public function warning(

        $message

    ) {

        $t = $this;

        $t

            ->getLogger()
            ->warning(

                $message

            );

        return $t;
    }

    public function getLogger()
    {

        $t = $this;

        return $t->logger;
    }

    public function build()
    {

        $t = $this;

        $t->logger    =

            new \Monolog\Logger(

                $t->getLoggerName()

            );

        $t

            ->logger
            ->pushHandler(

                new \Monolog\Handler\StreamHandler(

                    $t->getLoggerFilePath(),

                    \Monolog\Logger::WARNING

                )

            );

        return $t;
    }
}
