<?php

namespace ProdCalcFrameworkHelper\Validation;

use ProdCalcFrameworkHelper\RegExp;

class StringValidator extends BasicValidator

{
    public function validateBasicToken(

        $basicToken,
        $message

    ) {

        $t = $this;

        if (

            (preg_match(RegExp::BASIC_TOKEN, $basicToken) === 1)
        ) {

            return $t;
        }

        $t->addError(

            $message

        );

        return $t;
    }

    public function validateRegExp(

        $pattern,
        $subject,
        $message

    ) {

        $t = $this;

        if (

            (preg_match($pattern, $subject) === 1)
        ) {

            $t->addError(

                $message

            );

            return $t;
        }

        return $t;
    }
}
