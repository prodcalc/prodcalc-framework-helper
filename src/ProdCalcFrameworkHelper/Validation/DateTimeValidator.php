<?php

namespace ProdCalcFrameworkHelper\Validation;

use Respect\Validation\Validator as v;

class DateTimeValidator extends BasicValidator
{
    public function validateEqualOrFutureDate(

        $startDate,
        $finishDate,
        $message

    ) {

        $t  = $this;

        $t->validateDate(

            $startDate,
            $message

        );

        $t->validateDate(

            $finishDate,
            $message

        );

        if (

            $t->getHasError()

        ) {

            return $t;
        }

        if (

            $finishDate < $startDate

        ) {

            $t->addError(

                $message

            );
        }

        return $t;
    }

    public function validateCurrentOrFutureDate(

        $currentDate,
        $date,
        $message

    ) {

        $t  = $this;

        $t->validateDate(

            $date,
            $message

        );

        $t->validateDate(

            $currentDate,
            $message

        );

        if (

            $t->getHasError()

        ) {

            return $t;
        }

        if (

            $date < $currentDate

        ) {

            $t->addError(

                $message

            );
        }

        return $t;
    }

    public function validateDate(

        $date,
        $message

    ) {

        $t = $this;

        if (

            v::date()->validate(

                $date

            )
        ) {

            return $t;
        }

        $t->addError(

            $message

        );

        return $t;
    }

    public function validatePositiveInt(

        $quantity,
        $message

    ) {

        $t = $this;

        if (

            v::intVal()->validate(

                $quantity

            )

            && ($quantity > 0)

        ) {

            return $t;
        }

        $t->addError(

            $message

        );

        return $t;
    }
}
