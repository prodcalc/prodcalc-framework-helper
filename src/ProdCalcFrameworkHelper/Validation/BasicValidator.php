<?php

namespace ProdCalcFrameworkHelper\Validation;

abstract class BasicValidator
{

    private $errorList;
    private $hasError;

    public function addError(

        $message

    ) {

        $t = $this;

        $t->errorList[] = $message;

        $t->hasError = true;

        return $t;
    }

    public function setErrorList(

        $errorList

    ) {

        $t = $this;

        $t->errorList = $errorList;

        return $t;
    }

    public function getErrorList()
    {

        $t = $this;

        return $t->errorList;
    }

    public function clearErrorList()
    {

        $t = $this;

        $t->setErrorList(

            []

        );

        $t->setHasError(

            false

        );

        return $t;
    }

    public function setHasError(

        $hasError

    ) {

        $t = $this;

        $t->hasError = $hasError;

        return $t;
    }

    public function getHasError()
    {

        $t = $this;

        return $t->hasError;
    }

    public function link(

        $validator

    ) {

        $t = $this;

        $validator->setHasError(

            $t->getHasError()

        );

        $validator->setErrorList(

            $t->getErrorList()

        );

        return $validator;
    }

    public function __construct()
    {

        $t = $this;

        $t->clearErrorList();
    }
}
