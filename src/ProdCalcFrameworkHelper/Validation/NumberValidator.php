<?php

namespace ProdCalcFrameworkHelper\Validation;

use Respect\Validation\Validator as v;

class NumberValidator extends BasicValidator
{
    public function validatePositiveInt(

        $quantity,
        $message

    ) {

        $t = $this;

        if (

            v::intVal()->validate(

                $quantity

            )

            && ($quantity > 0)

        ) {

            return $t;
        }

        $t->addError(

            $message

        );

        return $t;
    }
}
